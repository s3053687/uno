package UNO.Model;

import UNO.Controller.ClientHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NetworkPlayer extends Player {
    private List<Card> hand;

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    private ClientHandler clientHandler;

    public String getName() {
        return name;
    }
    private String name;

    public NetworkPlayer(String name, ClientHandler clientHandler){
        super(name);
        this.name = name;
        this.clientHandler = clientHandler;
        hand = new ArrayList<Card>();
    }


    public Deck draw(Deck deck){
        Card card = deck.getCard();
        hand.add(card);
        deck.getCards().remove(card);
        return deck;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void playCard(Card card) throws IOException {
        if (hand.contains(card)) {
            hand.remove(card);
            clientHandler.broadcast(name + " played " + card + ".");
        } else{
            clientHandler.broadcast("Invalid card, this card is not in your hand ;(");
        }
    }

    public void invalidMove() {
        this.clientHandler.writeToClient("Card is not valid or does not exist, try again");
    }

    public int getNetworkPlayerChoice() throws IOException {
        return this.clientHandler.getInput();
    }

    public Card.Colour getNetworkColourChoice() throws IOException {
        clientHandler.broadcast("Choose a colour. r - red, g - green, b - blue, y - yellow");
        String input = this.getClientHandler().getStringInput();
        switch (input){
            case("r"):
                return Card.Colour.RED;
            case("b"):
                return Card.Colour.BLUE;
            case ("y"):
                return Card.Colour.YELLOW;
            case("g"):
                return Card.Colour.GREEN;
            default:
                getClientHandler().writeToClient("Incorrect colour choice :/");
                break;
        }
        return null;
    }


}
