package UNO.Model;

public class Card {
    public enum Colour {
        RED, BLUE, GREEN, YELLOW, WILD;
        private static final Colour[] colours = Colour.values();
    }

    public static Colour getColour(int i) {
        return Colour.colours[i];
    }

    public enum Value {
        ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, DRAW2, SKIP, REVERSE, DRAW4, CARD;
        private static final Value[] values = Value.values();
    }


    public static Value getValue(int i) {
        return Value.values[i];
    }

    private final Colour colour;
    private final Value value;

    public Card(Colour colour, Value value){
        this.colour = colour;
        this.value = value;
    }

    public Colour getColour() {
        return this.colour;
    }

    public Value getValue() {
        return this.value;
    }

    public String toString(){
        if (colour == Colour.WILD){
            return colour + "" + value;
        } else return colour + " " + value;
    }

}