package UNO.Model;

import UNO.Controller.ClientHandler;
import UNO.Controller.Server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;


public class GameOnline extends Game {

    /**
     * Creates a deck and a list of players for the game
     *
     * @param players
     */

    private final Socket socket;
    private final Server server;
    private final ClientHandler clientHandler;

    public GameOnline(ArrayList<Player> players, ClientHandler clientHandler, Socket socket, Server server) throws IOException {
        super(players);
        this.socket = socket;
        this.server = server;
        this.clientHandler = clientHandler;
    }


    /**
     * @return The player that has won the game round
     */
    @Override
    public Player isWinner() throws IOException {
        Player winner = null;
        if (isRoundOver()) {
            for (Player player : getPlayers()) {
                if (player.getHand().isEmpty()) {
                    winner = player;
                    clientHandler.broadcast(player.getName() + " has won this round.");
                }
            }
        }
        return winner;
    }

    public void playGame() throws IOException {
        while (!gameOver()) {
            for (Player p : getPlayers()) {
                p.getHand().clear();
            }
            deck = new Deck();
            deck.shuffle();
            for (Player p : getPlayers()) {
                for (int j = 0; j < 7; j++) {
                    p.draw(deck);
                }
            }
            System.out.println("Server starts a new round!");
            clientHandler.broadcast("NEW ROUND!");

            playRound();
        }
        clientHandler.broadcast("End of the game. The winner of the game is: " + gameWinner.getName() + "!!!!");
    }

    /**
     * Plays a game round
     */
    @Override
    public void endOfRoundMethod() throws IOException {
        clientHandler.broadcast(isWinner().getName() + " is the winner of this round!");
        clientHandler.broadcast("The current leaderboard:");

        calculatePoints();
        for (Player player : getPlayers()) {
            int playerPoints = 0;
            if (getLeaderboard().get(player) != null) {
                playerPoints = getLeaderboard().get(player);
            }
            clientHandler.broadcast(player.getName() + ": " + playerPoints + " points(s)");
        }
    }

    @Override
    public void printInstructions(Player currentP) throws IOException {
        if (currentP instanceof NetworkPlayer) {
            clientHandler.broadcast("TOP CARD: " + getCurrentCard());
            clientHandler.broadcast("It's " + currentP.getName() + "'s turn.");
            if (getCurrentCard().getValue() == Card.Value.DRAW4 || getCurrentCard().getValue() == Card.Value.CARD) {
                clientHandler.broadcast("The colour is now " + getColour().toString());
            }
            NetworkPlayer p = (NetworkPlayer) currentP;
            p.getClientHandler().writeToClient("To select a card to play, enter the number of the card");
            p.getClientHandler().writeToClient("If you want to draw a card, select 0");
        }
    }


    @Override
    public void printHand() {
        Player player = getPlayers().get(getCurrentPlayer());
        String hand = "";
        int i = 1;
        for (Card card : player.getHand()) {
            hand = hand + i + ": " + card.toString() + "||";
            i++;
        }
        if (player instanceof NetworkPlayer) {
            NetworkPlayer p = (NetworkPlayer) player;
            p.getClientHandler().writeToClient(hand);
        } else {
            System.out.println("Server writes to CP: " + hand);
        }
    }
}

