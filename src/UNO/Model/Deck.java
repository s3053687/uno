package UNO.Model;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

/*
108 cards
one 0
two 1,2,3,4,5,6,7,8,9,draw,block,reverse
four wild, wild4
*/

    public ArrayList<Card> getCards() {
        return cards;
    }
    public Card getCard(){
        return cards.get(cards.size()-1);
    }
    public Card getCardFromDiscard(){
        return discard.get(discard.size()-1);
    }


    private ArrayList<Card> cards = new ArrayList<>();
    private ArrayList<Card> discard = new ArrayList<>();


    public void shuffle(){
        Collections.shuffle(cards);
    }


    public Deck(){
        Card.Colour[] colours = Card.Colour.values();
        int cardsInDeck = 0;

        //loop is creating one card with value 0
        for (int i = 0; i <colours.length-1; i++){
            Card.Colour colour = colours[i];
            cards.add(cardsInDeck++, new Card(colour, Card.getValue(0)));

           //loop is creating two cards for each number up until 9
            for(int j=1; j<10; j++){
                for (int k = 0; k < 2; k++)/**/{
                    cards.add(cardsInDeck++, new Card(colour, Card.getValue(j)));
                }
            }

            Card.Value[] values = new Card.Value[]{
                    Card.Value.DRAW2, Card.Value.SKIP, Card.Value.REVERSE
            };

          //loop is creating two new cards for every special value
            for(Card.Value value : values) {
                for (int k = 0; k < 2; k++) {
                    cards.add(cardsInDeck++, new Card(colour, value));
                }
            }

        }

        Card.Value[] values = new Card.Value[]{
                Card.Value.CARD, Card.Value.DRAW4
        };

        for (Card.Value value : values){
            for (int k = 0 ; k<4 ; k++) {
                // Colour.WILD = four colours on the card
                cards.add(cardsInDeck++, new Card(Card.Colour.WILD, value));
            }
        }
    }

    public void addToDiscard(Card card){
        discard.add(card);
    }

    public ArrayList<Card> getDiscard() {
        return discard;
    }
}

