package UNO.Model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Player {
    private final List<Card> hand;
    private final String name;

    public Player(String name) {
        this.name = name;
        hand = new ArrayList<Card>();
    }

    public String getName() {
        return name;
    }

    public Deck draw(Deck deck) {
        Card card = deck.getCard();
        hand.add(card);
        deck.getCards().remove(card);
        return deck;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void playCard(Card card) throws IOException {
        if (hand.contains(card)) {
            hand.remove(card);
            System.out.println(name + " played " + card + ".");
        } else {
            System.out.println("Invalid card, this card is not in your hand ;(");
        }
    }

    public void invalidMove() {
        System.out.println("Card is not valid or does not exist, try again");
    }

    public int getPlayerChoice(Player player, Card currentCard, Card.Colour colour) throws IOException {
        if (player instanceof ComputerPlayer) {
            return (((ComputerPlayer) player).getComputerChoice(currentCard, colour));
        } else if (player instanceof HumanPlayer) {
            return ((HumanPlayer) player).getHumanPlayerChoice();
        } else if (player instanceof NetworkPlayer) {
            return ((NetworkPlayer) player).getNetworkPlayerChoice();
        }
        return -1;
    }

    public Card.Colour getColourChoice(Player player) throws IOException {
        if (player instanceof ComputerPlayer) {
            return (((ComputerPlayer) player).chooseColour());
        } else if (player instanceof HumanPlayer) {
            return ((HumanPlayer) player).getHumanColourChoice();
        } else if (player instanceof NetworkPlayer) {
            return ((NetworkPlayer) player).getNetworkColourChoice();
        }
        return null;
    }
}
