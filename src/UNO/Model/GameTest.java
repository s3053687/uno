//package UNO.Model;
//
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class GameTest {
//    private List<String> players = new ArrayList<>();
//
//    @Test
//    public void testGetCurrentPlayer() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//
//        assertEquals(0, game.getCurrentPlayer());
//    }
//
//    @Test
//    public void testSetDeck() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//        Deck deck = new Deck();
//
//        game.setDeck(deck);
//        assertEquals(deck, game.getDeck());
//    }
//
//    @Test
//    public void testGetPlayers() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//
//        assertEquals(players, game.getPlayers());
//    }
//
//    @Test
//    public void testGetPlayer() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//
//        assertEquals(players.get(0), game.getPlayer(0));
//    }
//
//
//    @Test
//    public void testSetCurrentPlayer() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//
//        game.setCurrentPlayer(1);
//        assertEquals(1, game.getCurrentPlayer());
//    }
//
//    @Test
//    public void testGetLeaderboard() {
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(new Player("Player 1"));
//        players.add(new Player("Player 2"));
//        Game game = new Game(players);
//
//        assertNotNull(game.getLeaderboard());
//    }
//
////    @Test
////    public void testGameConstructor() {
////        assertNotNull(game);
////        assertNotNull(game.getDeck());
////        assertNotNull(game.getPlayers());
////        assertEquals(0, game.getCurrentPlayer());
////        assertEquals(true, game.getClockWise());
////    }
//
//}
