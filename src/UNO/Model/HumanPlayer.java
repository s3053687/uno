package UNO.Model;

import java.io.IOException;
import java.util.Scanner;

public class HumanPlayer extends Player{
    public HumanPlayer(String name) {
        super(name);
    }

    /**
     * Gets the player's choice of which card they would like to play.
     * @return the index of the chosen card
     */
    public int getHumanPlayerChoice() throws IOException {
        Scanner scanner = new Scanner(System.in);
        try{
            String choice = scanner.next();
            return Integer.parseInt(choice);
        } catch (NumberFormatException e ){
            System.out.println("Invalid input, please enter a number");
            return getHumanPlayerChoice();
        }
    }

    public Card.Colour getHumanColourChoice(){
        System.out.println("Choose a colour. r - red, g - green, b - blue, y - yellow");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        switch (input){
            case("r"):
                return Card.Colour.RED;
            case("b"):
                return Card.Colour.BLUE;
            case ("y"):
                return Card.Colour.YELLOW;
            case("g"):
                return Card.Colour.GREEN;
            default:
                System.out.println("Incorrect colour choice");
                break;
        }
        return null;
    }

}
