package UNO.Model;

public class ComputerPlayer extends Player {

    Game game;
    Deck deck = new Deck();

    public ComputerPlayer(String name) {
        super(name);
    }

    public int getComputerChoice( Card topCard, Card.Colour colour) {
        Integer card = 0;
        card = validCardChoice(topCard, colour);
        if(card == -1){
            card = lookForWild();
        }
        return card;
    }

    private Integer validCardChoice(Card topCard, Card.Colour colour) {
        for (Card card : getHand()) {
            if (isCardValidComputer(card, topCard, colour)) {
                return getHand().indexOf(card) + 1;
            }
        }
        return -1;
    }

    private Integer lookForWild(){
        for (Card card : getHand()){
            if (isWildCardValid(card)){
                return getHand().indexOf(card)+1;
            }
        }
        return 0;
    }

    private boolean isCardValidComputer(Card card, Card currentCard, Card.Colour colour) {
        if(card.getColour() == currentCard.getColour() || card.getColour() == colour){
            return true;
        } else if(card.getValue() == currentCard.getValue()){
            return true;
        }
        return false;
    }

    private boolean isWildCardValid(Card card){
        if (card.getValue() == Card.Value.CARD || card.getValue() == Card.Value.DRAW4){
            return true;
        }
        return false;
    }

    public Card.Colour chooseColour() {
        return Card.Colour.RED;
    }
}


