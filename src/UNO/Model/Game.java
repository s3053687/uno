package UNO.Model;

import java.io.IOException;
import java.util.*;

public class Game {
    public Deck deck;
    private List<Player> players;
    private int currentPlayer;
    private Card currentCard;
    public Deck discardedPile = new Deck();
    public Map<Player, Integer> leaderboard = new HashMap<Player, Integer>();
    private Card.Colour colour;
    public boolean clockWise;
    private Player currentP;
    private List<String> computerPlayers = new ArrayList<>();
    public Player gameWinner;


    // ------------------------------- getters and setters ------------------------------------------
    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public Deck getDeck() {
        return deck;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getPlayer(int currentPlayer) {
        return players.get(currentPlayer);
    }

    public Card getCurrentCard() {
        return currentCard;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public void setCurrentCard(Card currentCard) {
        this.currentCard = currentCard;
    }

    public Map<Player, Integer> getLeaderboard() {
        return leaderboard;
    }

    public Card.Colour getColour() {
        return this.colour;
    }

    public void setColour(Card.Colour colour) {
        this.colour = colour;
    }

//    public Player getCurrentP() {
//        return players.get(currentPlayer);
//    }

    public boolean getClockWise() {
        return clockWise;
    }

    public void setClockWise(boolean clockWise) {
        this.clockWise = clockWise;
    }

    // --------------------------- constructor -------------------------------

    /**
     * Creates a deck and a list of players for the game
     */
    public Game(ArrayList<Player> players) {
        computerPlayerMethod(players);
        deck = new Deck();
        deck.shuffle();
        this.players = players;
        currentPlayer = 0;
        clockWise = true;
    }

// ------------------------------- methods -----------------------------------

    public void playGame() throws IOException {
        while (!gameOver()) {
            for (Player p : players) {
                if (p.getHand().size() != 0) {
                    p.getHand().removeAll(p.getHand());
                }
            }
            deck = new Deck();
            deck.shuffle();
            for (Player p : players) {
                for (int j = 0; j < 7; j++) {
                    p.draw(deck);
                }
            }
            playRound();
        }
        System.out.println("End of game. The winner of the game is: " + gameWinner.getName() + "!!!!");
    }

    /**
     * Plays a game round
     */
    public void playRound() throws IOException {
        Card topCard = getTopCardMethod();
        currentCard = topCard;
        int playerChoice;
        while (!isRoundOver()) {
            if (currentCard.getValue() == Card.Value.SKIP) {
                currentPlayer = getNextPlayer(currentPlayer);
            }
            if (currentCard.getValue() == Card.Value.DRAW4) {
                wildFourAction();
            }
            if (currentCard.getValue() == Card.Value.DRAW2) {
                drawTwoAction();
            }
            printHand();
            printInstructions(players.get(currentPlayer));
            playerChoice = players.get(currentPlayer).getPlayerChoice(players.get(currentPlayer), currentCard, colour);
            playAction(playerChoice);
            if (playerChoice == 0) {
                currentPlayer = getPreviousPlayer(getNextPlayer(currentPlayer));
            }
            changeOrderMethod();
        }

        endOfRoundMethod();
    }

    public void createPlayers() throws IOException {
        Scanner scanner = new Scanner(System.in);
        int playerAmount = 0;
        System.out.println(("How many players do you want to play with? You can play with a minimum of 2 players and a maximum of 10 players"));
        try {
            playerAmount = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid input, please enter a number between 2 and 10");
        }
        if (!(playerAmount >= 2 && playerAmount <= 10)) {
            System.out.println("Invalid input, please enter a number between 2 and 10");
            playerAmount = 0;
            createPlayers();
        }
        for (int i = 0; i < playerAmount; i++) {
            String name = "";
            System.out.println("What is player " + (i + 1) + " name");
            name = scanner.next();
            if (name.equals("CP")) {
                ComputerPlayer computerPlayer = new ComputerPlayer(name);
                getPlayers().add(computerPlayer);
            } else {
                Player player = new Player(name);
                players.add(player);
            }
        }
        for (Player p : players) {
            for (int j = 0; j < 7; j++) {
                p.draw(deck);
            }
        }

    }


    /**
     * @return The player that has won the game round
     */
    public Player isWinner() throws IOException {
        Player winner = null;
        if (isRoundOver()) {
            for (Player player : players) {
                if (player.getHand().isEmpty()) {
                    winner = player;
                }
            }
        }
        return winner;
    }

    public void playAction(int playerChoice) throws IOException {
        //computerPlayer
        Player p = players.get(currentPlayer);
        if (!(playerChoice <= players.get(currentPlayer).getHand().size())) {
            p.invalidMove();
            playAction(p.getPlayerChoice(players.get(currentPlayer), currentCard, colour));
        } else if (playerChoice == 0) {
            if (deck.getCards().size() < 5) {
                discardedPile.shuffle();
                deck = discardedPile;
            }
            deck = players.get(currentPlayer).draw(deck);

        } else if (players.get(currentPlayer).getHand().size() >= playerChoice && isCardValid(players.get(currentPlayer).getHand().get(playerChoice - 1))) {
            Card c = players.get(currentPlayer).getHand().get(playerChoice - 1);
            reverseCardAction(c);
            if (wildColourValidation(c)) {
                playCardAction(c);
            }

        }
    }

    public void computerPlayerMethod(ArrayList<Player> players) {
        for (Player player : players) {
            String[] splitter = player.getName().split("::");
            if (splitter[0] == "CP") {
                computerPlayers.add(player.getName());
            }
        }
    }

    /**
     * @param card the card that the player would like to play
     * @returns true if the preferred card is a valid play
     * @returns false if the preferred card is not a valid play
     */
    public boolean isCardValid(Card card) {
        if (currentCard.getValue() == Card.Value.CARD || currentCard.getValue() == Card.Value.DRAW4) {
            return card.getColour() == colour;
        }
        if (card.getValue() == currentCard.getValue()) {
            return true;
        }
        if (card.getColour() == currentCard.getColour()) {
            return true;
        }
        if (card.getValue() == Card.Value.CARD || card.getValue() == Card.Value.DRAW4) {
            return true;
        }
        return false;
    }

    /**
     * Finds which player is after the current player, while paying attention to the end of the ArrayList.
     *
     * @param player the index of the current player
     * @return the next player index that is after the current player
     */
    public int getNextPlayer(int player) {
        if ((player + 1) < players.size()) {
            return (player + 1);
        } else {
            return 0;
        }
    }

    /**
     * Finds which player is before the current player, while paying attention to the end of the ArrayList.
     *
     * @param player the index of the current player
     * @return the previous player index that is after the current player
     */
    public int getPreviousPlayer(int player) {
        if ((player - 1) < 0) {
            return players.size() - 1;
        } else {
            return player - 1;
        }
    }

    /**
     * Asks the player which card they would like to choose.This function is used when a WILD card is played.
     *
     * @return The player's chosen colour.
     */


    public void playCardAction(Card c) throws IOException {
        players.get(currentPlayer).playCard(c);
        discardedPile.addToDiscard(c);
        currentCard = c;
        if (c.getColour() != Card.Colour.WILD) {
            this.colour = c.getColour();
        }
    }

    public Card getTopCardMethod() {
        Card topCard = deck.getCard();
        deck.getCards().remove(topCard);
        deck.addToDiscard(topCard);


        while (topCard.getValue() == Card.Value.DRAW4 || topCard.getValue() == Card.Value.CARD) {
            topCard = deck.getCard();
            deck.getCards().remove(topCard);
            deck.addToDiscard(topCard);
        }
        return topCard;
    }

    // ------------------------------ printing methods ----------------------------------------------------

    public void printInstructions(Player currentP) throws IOException {
        System.out.println("\nIt's " + currentP.getName() + "'s turn.");
        System.out.println("Top Card: " + currentCard);
        if (currentCard.getValue() == Card.Value.DRAW4 || currentCard.getValue() == Card.Value.CARD) {
            System.out.println("The colour is now " + this.colour.toString());
        }
        System.out.println("To select a card to play, enter the number of the card");
        System.out.println("If you want to draw a card, select 0");
    }

    public void printHand() throws IOException {
        if (players.get(currentPlayer).getHand().size() == 0) {
            System.out.println("hi");
        }
        for (Card card : players.get(currentPlayer).getHand()) {
            System.out.print(players.get(currentPlayer).getHand().indexOf(card) + 1 + ": " + card.toString() + " | ");
        }
    }

    // --------------------------------- special card actions -----------------------------------------

    public void drawTwoAction() {
        players.get(currentPlayer).draw(deck);
        players.get(currentPlayer).draw(deck);
        currentPlayer = getNextPlayer(currentPlayer);
    }

    public void reverseCardAction(Card c) {
        if (c.getValue() == Card.Value.REVERSE) {
            if (clockWise) {
                clockWise = false;
            } else clockWise = true;
        }
    }

    public void changeOrderMethod() {
        if (clockWise) {
            if (currentPlayer != players.size() - 1) {
                currentPlayer = currentPlayer + 1;
            } else {
                currentPlayer = 0;
            }
        } else {
            if (currentPlayer != 0) {
                currentPlayer = currentPlayer - 1;
            } else {
                currentPlayer = players.size() - 1;
            }
        }
    }

    public boolean wildColourValidation(Card c) throws IOException {
        boolean result = true;
        if (c.getValue() == Card.Value.CARD) {
            colour = players.get(currentPlayer).getColourChoice(players.get(currentPlayer));
            if (colour == null) {
                wildColourValidation(c);
            }
        }
        if (c.getValue() == Card.Value.DRAW4) {
            boolean cardFound = false;
            for (Card c1 : players.get(currentPlayer).getHand()) {
                if (c1.getColour().equals(colour)) {
                    cardFound = true;
                }
            }
            if (cardFound) {
                System.out.println("You cannot play a wild card since you have a card of the same colour as the topCard in your hand. You will have another turn");
                currentPlayer = getPreviousPlayer(currentPlayer);
                result = false;
            } else {
                colour = players.get(currentPlayer).getColourChoice(players.get(currentPlayer));
                if (colour == null) {
                    wildColourValidation(c);
                }
            }
        }
        return result;
    }

    public void wildFourAction() {
        players.get((currentPlayer)).draw(deck);
        players.get((currentPlayer)).draw(deck);
        players.get((currentPlayer)).draw(deck);
        players.get((currentPlayer)).draw(deck);
        currentPlayer = getNextPlayer(currentPlayer);
    }

    // ---------------------------- end of game/round methods ---------------

    /**
     * Calculates the points the winner of the game round has achieved and adds this information to the leaderboard
     */
    public void calculatePoints() throws IOException {
        int pts = 0;
        int totalPoints = 0;
        for (Player p : players) {
            for (Card card : p.getHand()) {
                if (card.getColour() == Card.Colour.WILD) {
                    totalPoints += 50;
                } else if (card.getValue() == Card.Value.DRAW2 || card.getValue() == Card.Value.REVERSE || card.getValue() == Card.Value.SKIP) {
                    totalPoints += 20;
                } else if (card.getValue() == Card.Value.ZERO) {
                    totalPoints += 0;
                } else if (card.getValue() == Card.Value.ONE) {
                    totalPoints += 1;
                } else if (card.getValue() == Card.Value.TWO) {
                    totalPoints += 2;
                } else if (card.getValue() == Card.Value.THREE) {
                    totalPoints += 3;
                } else if (card.getValue() == Card.Value.FOUR) {
                    totalPoints += 4;
                } else if (card.getValue() == Card.Value.FIVE) {
                    totalPoints += 5;
                } else if (card.getValue() == Card.Value.SIX) {
                    totalPoints += 6;
                } else if (card.getValue() == Card.Value.SEVEN) {
                    totalPoints += 7;
                } else if (card.getValue() == Card.Value.EIGHT) {
                    totalPoints += 8;
                } else if (card.getValue() == Card.Value.NINE) {
                    totalPoints += 9;
                }
            }
        }
        if (leaderboard.containsKey(isWinner())) {
            pts = leaderboard.get(isWinner()) + totalPoints;
        } else pts = totalPoints;
        leaderboard.put(isWinner(), pts);
    }

    public boolean isRoundOver() {
        for (Player player : players) {
            if (player.getHand().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public boolean gameOver() {
        for (Player p : players) {
            if (leaderboard.get(p) != null && leaderboard.get(p) >= 500) {
                gameWinner = p;
                return true;
            }
        }
        return false;
    }


    public void endOfRoundMethod() throws IOException {
        System.out.println(isWinner().getName() + " is the winner of this round!");
        System.out.println("The current leaderboard:");
        calculatePoints();
        for (Player player : players) {
            int playerPoints = 0;
            if (leaderboard.containsKey(player)) {
                playerPoints = leaderboard.get(player);
            }
            System.out.println(player.getName() + ": " + playerPoints + " point(s)");
        }
    }

}
