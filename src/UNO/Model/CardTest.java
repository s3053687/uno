//package UNO.Model;
//
//
//import UNO.Model.Card;
//import org.testng.annotations.Test;
//
//
//import static UNO.Model.Card.Colour.RED;
//import static org.testng.AssertJUnit.assertEquals;
//
//
//public class CardTest {
//    Card card = new Card(RED, Card.Value.ONE);
//    @Test
//    public void testGetColor(){
//        assertEquals(RED, card.getColour());
//    }
//
//    @Test
//    public void testGetValue(){
//        assertEquals(Card.Value.ONE, card.getValue());
//    }
//
//    @Test
//    public void testToString(){
//        assertEquals("RED ONE", card.toString());
//    }
//
//}
