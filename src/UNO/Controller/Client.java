package UNO.Controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {

    private Socket socket;
    private ServerHandler handler;
    private String clientName;

    private final BufferedReader in;
    private ArrayList<String> usernames = new ArrayList<>();

    public Client(String clientName) {
        this.clientName = clientName;
        this.in = new BufferedReader(new InputStreamReader(System.in));
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name if you would like to join the queue ");
        String clientName = scanner.nextLine();
        Client client = new Client(clientName);
        client.start("localhost", 5555);
    }

    public ArrayList<String> getUsernames() {
        return usernames;
    }

    public String getClientName() {
        return clientName;
    }

    public void closeEverything(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(String host, int port) {
        try {
            this.socket = new Socket(host, port);
            this.handler = new ServerHandler(socket, this);
            new Thread(handler).start();
            handler.writeToServer(Protocol.JOIN + Protocol.COMMON_SEPERATOR + getClientName());
            while (socket.isConnected()) {
                String input = in.readLine();
                handler.handleInput(input);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
