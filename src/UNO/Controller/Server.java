package UNO.Controller;

import UNO.Model.GameOnline;
import UNO.Model.Player;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {
    public GameOnline game;
    private ServerSocket serverSocket;
    private boolean gameInitiated = false;
    private ArrayList<String> joinedPlayers = new ArrayList<>();

    private ArrayList<ClientHandler> clientHandlers;
    private ArrayList<Player> players = new ArrayList<>();

    public Server(ServerSocket serverSocket) throws IOException {
        this.serverSocket = serverSocket;
        this.clientHandlers = new ArrayList<>();
    }

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(5555);
        Server server = new Server(serverSocket);
        Thread thread = new Thread(server);
        thread.start();
    }

    public boolean isGameInitiated() {
        return gameInitiated;
    }

    public ArrayList<ClientHandler> getClientHandlers() {
        return clientHandlers;
    }

    public void setClientHandlers(ArrayList<ClientHandler> clientHandlers) {
        this.clientHandlers = clientHandlers;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<String> getJoinedPlayers() {
        return joinedPlayers;
    }

    public void setJoinedPlayers(ArrayList<String> joinedPlayers) {
        this.joinedPlayers = joinedPlayers;
    }

    public GameOnline getGame() {
        return game;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("A new client has connected!");
                ClientHandler clientHandler = new ClientHandler(socket, this);
                clientHandlers.add(clientHandler);
                Thread threadCH = new Thread(clientHandler);
                threadCH.start();
            }
        } catch (IOException e) {
            closeServerSocket();
        }

    }

    public void closeServerSocket() {
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

