package UNO.Controller;

import UNO.Model.ComputerPlayer;
import UNO.Model.GameOnline;
import UNO.Model.NetworkPlayer;
import UNO.Model.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class ClientHandler implements Runnable {
    public int input = -1;
    public String stringInput = "";
    private final Socket socket;
    private final BufferedWriter out;
    private final BufferedReader in;
    private Player player;
    private String clientUsername;
    private final Server server;
    private GameOnline gameOnline;
    private Integer move;
    private String name;

    public ClientHandler(Socket socket, Server server) throws IOException {
        this.socket = socket;
        this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.server = server;
    }

    public int getInput() {
        input = -1;
        switchCase();
        return input;
    }

    public String getStringInput() {
        stringInput = "";
        switchCase();
        return stringInput;
    }

    public void writeToClient(String message) {
        System.out.println("Server writes to " + this.clientUsername + ": " + message);
        try {
            out.write(message);
            out.newLine();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void run() {
        switchCase();
    }

    public void switchCase() {
        try {
            String input;
            while ((input = in.readLine()) != null) {
                System.out.println("Server input: " + input);
                String[] commands = input.split("::");
                switch (commands[0]) {
                    case (Protocol.JOIN):
                        clientUsername = commands[1];
                        handleJoin(clientUsername);
                        System.out.println(clientUsername + " has joined to participate in the game");
                        break;
                    case (Protocol.CLIENTREADY):
                        handleReady(commands[1]);
                        if (server.getJoinedPlayers().size() == server.getPlayers().size()) {
                            // Add a CP if there is just one player
                            if (server.getPlayers().size() == 1) {
                                handleReady("CP");
                            }
                            ArrayList<String> players = new ArrayList();
                            for (Player p : server.getPlayers()) {
                                players.add(p.getName());
                            }
                            writeToClient(Protocol.SERVERREADY + Protocol.COMMON_SEPERATOR + players);
                            System.out.println("Server: Starting the game!");
                            startGameOnline();
                        } else {
                            // The player is ready, so we don't need this thread anymore
                            System.out.println("Server: waiting for other players...");
                            return;
                        }
                        break;
                    case (Protocol.PLAYMOVE):
                        handleInput(commands[1]);
                        return;
                    case (Protocol.NEXTTURN):
                        String hand = commands[1];
                        String newTopCard = commands[2];
                        String currentPlayer = commands[3];
                        handleNextTurn(hand, currentPlayer, newTopCard);
                        break;
                    case (Protocol.ERROR):
                        String errorCode = commands[1];
                        handleError(errorCode);
                        break;
                    case (Protocol.ROUNDDONE):
                        String username = commands[1];
                        String points = commands[2];
                        handleRoundDone(username, points);
                        break;
                    case (Protocol.GAMEENDED):
                        String winner = commands[1];
                        handleGameEnded(winner);
                        break;

                    default:
                        System.out.println(input);
                }
            }
        } catch (IOException e) {
            if (e.getMessage().equals("Connection reset")) {
                System.out.println(player.getName() + " disconnected");
                closeEverything();
            }
        }
    }


    private void handleReady(String readyUsername) throws IOException {
        System.out.println("Server: Player " + readyUsername + " is ready!");
        if (readyUsername.equals("CP")) {
            ComputerPlayer computerPlayer = new ComputerPlayer(readyUsername);
            server.getPlayers().add(computerPlayer);
        } else {
            NetworkPlayer player = new NetworkPlayer(readyUsername, this);
            server.getPlayers().add(player);
        }
    }

    private void handleJoin(String clientUsername) throws IOException {
        server.getJoinedPlayers().add(clientUsername);
        String players = "";
        for (String player : server.getJoinedPlayers()) {
            if (players.length() == 0) {
                players = player;
            } else players = players + " " + player;
        }
        writeToClient(Protocol.JOINED + Protocol.COMMON_SEPERATOR + players);
    }

    private void startGameOnline() throws IOException {
        GameOnline game = new GameOnline(server.getPlayers(), this, socket, server);
        game.playGame();
    }

    private void handleNextTurn(String hand, String newTopCard, String currentPlayer) throws IOException {
        System.out.println("your hand: " + hand);
        System.out.println("Top Card: " + newTopCard);
        System.out.println("it is " + currentPlayer + "'s turn.");
        System.out.println("to play type PLAY::(index)");
    }

    private void handleGameEnded(String winner) throws IOException {
        broadcast(winner + " is the winner of the game!");
    }

    private void handleError(String errorCode) throws IOException {
        System.out.println(errorCode);
    }

    private void handleRoundDone(String username, String points) throws IOException {
        broadcast(username + " has won this rond!");
        broadcast("points: " + points);
    }

    private void handleServerReady(String readyNames) throws IOException {
        System.out.println("Players ready: " + readyNames);
        System.out.println("to start the game, type START");
    }

    public void broadcast(String message) throws IOException {
        for (Player p : this.server.getPlayers()) {
            System.out.println("Server write to " + p.getName() + ": " + message);
            if (p instanceof NetworkPlayer) {
                NetworkPlayer np = (NetworkPlayer) p;
                ClientHandler s = np.getClientHandler();
                s.out.write(message);
                s.out.newLine();
                s.out.flush();
            }
        }
    }

    public void closeEverything() {
        try {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String getName() {
        return name;
    }

    public void handleInput(String message) {
        try {
            this.input = parseInt(message);
        } catch (NumberFormatException e) {
            this.stringInput = message;
        }
    }
}
