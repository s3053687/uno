package UNO.Controller;

/**
 * Protocol for the mentor group of Jurgis, Arthur and Oskar
 * 
 * This is the standard protocol the mentor group will use in their game of UNO.
 * Please make sure to stay up to date with changes made to this document. Whenever something changes,
 * you will get a ping in our mentoring group channel. If there are questions or remarks, please ask them
 * in the corresponding thread of ms Teams.
 *
 * The full explanation and documentation of all commands can be found in the protocol description file
 * which is uploaded to ms teams.
 */
public class Protocol  {

	//--------------General Implementation--------------//

	/**
	 * the seperator used for indicating the command and different arguments
	 */
	public static final String COMMON_SEPERATOR = "::";

	/**
	 * the seperator used between different cards in a hand.
	 * ->Be aware, the regex needed in the String.split command is "//|//|"*/
	public static final String CARD_SEPERATOR = "||";
	

	/**
	 * the seperator used for splitting the color of the card and the type of the card
	 */
	public static final String COLOR_TYPE_SEPERATOR = " ";

	/**
	 * enum containing all possible colors
	 */
	public enum color{
		RED, BLUE, YELLOW, GREEN
	}

	/**
	 * enum containing all possible types of cards
	 */
	public enum	cardType{
		ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, //number cards
		SKIP, REVERSE, DRAW2, //action cards
		WILDCARD, WILDDRAW4 //wild cards
	}
	
	/**
	 * the maximum amount of charachters a name can be. the username should also not contain the common seperator
	 */
	public static final int MAX_USERNAME_CHARACTERS = 15;
	//--------------------------------------------------//
	
	//-----------------Joining A Server-----------------//
	/** The first message sent by the client to the server, indicating they want to join the game */
	public static final String JOIN = "JOIN";

	/** Broadcast by the server to all clients, displaying all the currently joined players */
	public static final String JOINED = "JOINED";
	//--------------------------------------------------//
	
	//-----------------Starting A Game------------------//
	/**	The message is sent by each client when they are ready. This should include their username again for ease of use*/
	public static final String CLIENTREADY = "CLIENTREADY";

	/** Broadcast by the server to all clients, displaying all the currently ready players*/
	public static final String SERVERREADY = "SERVERREADY";

	//--------------------------------------------------//

	//-----------------Playing The Game-----------------//
	/** As soon as all players who are joined, also indicated that they are ready, the server starts the game/round
	 * A START message is send to all players, containing the cards in their hand, the starting top card and the player who starts*/
	public static final String START = "START";

	/**
	 * should contain the move a player wants to play in the form of an index from 1 through the amount of cards in the hand.
	 * When the chosen index is a WILDCARD or WILDDRAW4, the client should also include the color they want to pick.
	 * if the index 0 is chosen, the player decides to pass and draw a card.
	 * finally The name of the player who plays the move also has to be added, so it is easy for the server to check if the right player is making a move.
	 */
	public static final String PLAYMOVE = "PLAYMOVE";

	/**
	 * This is send after a move, to the player who needs to draw cards. The DRAW message gives the complete hand of the client it is send to.
	 */
	public static final String DRAW = "DRAW";

	/**
	 * This message is send after a valid move has been played, and the DRAW message has been send. This message includes the new player who's
	 * turn it is and the new Top card.
	 */
	public static final String NEXTTURN = "NEXTTURN";

	/**
	 * this message is sent as soon as a player has 0 cards left. The message contains the name of the player who won the round, and the current standings.
	 * a new round is started by resetting the game, and invoking START again. This is only done when no players have reached the threshold amount of points yet.
	 */
	public static final String ROUNDDONE = "ROUNDDONE";

	/**	This may be sent by the client to indicate that they are disconnecting from the server. The message should include the username of the player who left,
	 * the other players should be informed and the server stops the game totally*/
	public static final String ABORT = "ABORT";

	/** This message is broadcast by the server to let all players know the game is over, and should include the username of the winning player*/
	public static final String GAMEENDED = "GAMEENDED";
	//--------------------------------------------------//

	//----------------------Errors----------------------//
	/** When a player causes an error to occur in the server, this message in sent to the player who caused the error, and it should be handled correctly. */
	public static final String ERROR = "ERROR";

	/** Array of all possible errors that can occur when using the protocol.
	 * -> This is private since it should not be used outside of this class*/
	private static final String[] ERRORNAMES = {"DUPLICATE_NAME", "INVALID_MOVE", "UNRECOGNIZED", "LOBBY_FULL", "NO_MORE_CARDS"};

	public static final String DUPLICATE_NAME = ERRORNAMES[0];
	public static final String INVALID_MOVE = ERRORNAMES[1];
	public static final String UNRECOGNIZED = ERRORNAMES[2];
	public static final String LOBBY_FULL = ERRORNAMES[3];
	public static final String NO_MORE_CARDS = ERRORNAMES[4];
	//--------------------------------------------------//
}
