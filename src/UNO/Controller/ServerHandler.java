package UNO.Controller;

import UNO.Model.GameOnline;
import UNO.Model.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class ServerHandler implements Runnable {
    //    private ArrayList<String> joinedPlayers = new ArrayList<>();
    private ArrayList<Player> readyPlayers = new ArrayList<>();
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private Scanner userInput = new Scanner(System.in);
    //    private String clientUsername;
    private GameOnline game;
    private String players;
    private Client client;

    public ServerHandler(Socket socket, Client client) throws IOException {
        try {
            this.socket = socket;
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.client = client;
        } catch (IOException e) {
            e.printStackTrace();
            closeEverything(socket, in, out);
        }
    }

    @Override
    public void run() {
        try {
            while (socket.isConnected()) {
                String input = in.readLine();
                System.out.println(this.client.getClientName() + " input: " + input);
                String[] commands = input.split("::");
                if (input.equals("exit")) {
                    break;
                }
                switch (commands[0]) {
                    case (Protocol.JOINED):
                        for (String s : commands[1].split(" ")) {
                            client.getUsernames().add(s);
                        }
                        handleJoined(client.getUsernames());
                        break;
                    case (Protocol.SERVERREADY):
                        System.out.println("Client: Server is ready!");
                        writeToServer(Protocol.START);
                        break;
                    case (Protocol.START):
                        writeToServer(Protocol.NEXTTURN + Protocol.COMMON_SEPERATOR + game.getPlayer(game.getCurrentPlayer()).getHand()
                                + Protocol.COMMON_SEPERATOR + game.getCurrentCard() + Protocol.COMMON_SEPERATOR + game.getPlayer(game.getCurrentPlayer()));
                        break;
                    case (Protocol.DRAW):
                        writeToServer(Protocol.DRAW + Protocol.COMMON_SEPERATOR + game.getPlayer(game.getCurrentPlayer()).getHand());
                        break;
                    case (Protocol.NEXTTURN):
                        writeToServer(Protocol.NEXTTURN + Protocol.COMMON_SEPERATOR + game.getPlayer(game.getCurrentPlayer()));
                        break;

                    case (Protocol.ERROR):
                        // to do
                        break;

                    case (Protocol.ROUNDDONE):
                        writeToServer(Protocol.ROUNDDONE + Protocol.COMMON_SEPERATOR + game.isWinner().getName() + Protocol.COMMON_SEPERATOR
                                + game.getLeaderboard().get(game.isWinner()));
                        break;

//                    case (Protocol.ABORT):
//                        clientUsername = commands[1];
//                        handleAbort(clientUsername);
//                        break;

                    case (Protocol.GAMEENDED):
                        //to do
                        break;

                }
            }
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    private void handlePlayMove(String move) throws IOException {
        game.playAction(parseInt(move));
    }

    public void handleJoined(ArrayList<String> clientUsernames) throws IOException {
        System.out.println("Players Joined: ");
        System.out.println(clientUsernames);
        System.out.println("Are you ready to play? Type yes or no.");
    }

    public void writeToServer(String message) {
        System.out.println("Client writes to server: " + message);
        if (out != null) {
            try {
                out.write(message);
                out.newLine();
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("unable to write to server");
        }
    }

    public void handleInput(String input) {
        String[] commands = input.split(" ");
        switch (commands[0]) {
            case ("yes"):
                writeToServer(Protocol.CLIENTREADY + Protocol.COMMON_SEPERATOR + client.getClientName());
                break;

            case ("play"): {
                writeToServer(Protocol.PLAYMOVE + Protocol.COMMON_SEPERATOR + commands[1]);
                break;
            }
            default:
                System.out.println("Cant understand " + input + ", please try again!");
        }

    }

    public void removeClientHandler() {
        writeToServer("SERVER: " + client.getClientName() + " has left the chat");
    }

    public void closeEverything(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {
        removeClientHandler();
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
