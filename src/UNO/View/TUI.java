package UNO.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import UNO.Model.ComputerPlayer;
import UNO.Model.Game;
import UNO.Model.HumanPlayer;
import UNO.Model.Player;

public class TUI {



    public static void main(String[] args) throws IOException {

        ArrayList<Player> players = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        int playerAmount=0;
        System.out.println("How many players do you want to play with? You can play with a minimum of 2 players and a maximum of 10 players");
            do {
                try {
                playerAmount = Integer.parseInt(scanner.next());
                if (!(playerAmount >= 2 && playerAmount <= 10)) {
                    System.out.println("Invalid input, please enter a number between 2 and 10. Try again!");
                    continue;
                }
                } catch (Exception e) {
                        System.out.println("Something went wrong! Try again!");
                        continue;
                    }
                break;
        } while (true);
        for (int i = 0; i<playerAmount; i++){
            String name = "";
            System.out.println("What is player " + (i+1) + " name?");
            name = scanner.next();
            String[] splitter = name.split("::");
            if(splitter[0].equals("CP")){
                Player computerPlayer = new ComputerPlayer(name);
                players.add(computerPlayer);
            }else{
                Player player = new HumanPlayer(name);
                players.add(player);
            }
        }
        Game game = new Game(players) {};

        game.playGame();
    }
}
